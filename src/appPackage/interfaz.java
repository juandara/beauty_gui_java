/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appPackage;

import ClasesVarias.MachineComands;
import ClasesVarias.Configuraciones.Configuraciones;
import ClasesVarias.Graficas;
import ClasesVarias.funcionesVarias;
import ClasesVarias.Configuraciones.ManejoDeConfiguraciones;
import ClasesVarias.PanelDeProgreso.ProgresBarRunnable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.jfree.chart.ChartPanel;
import org.jfree.data.time.Millisecond;

/**
 *
 * @author roberto
 */
public class interfaz extends javax.swing.JFrame {
    
    final String on ="on";
    final String off ="off";
    
    //aqui almaceno las configuraciones:        
    public static boolean notificaciones;//si
    public static double wattHora; //pesos
    public static double metrosCubicos;//pesos
    public static int numUsuarios;//numero de peronas en la casa
    public static int diaFacturacion;//15 de cada mes 
    public static ImageIcon imagenFondo;//inagen de fondo
    
    //objeto donde se almacenaran las configuraciones
    Configuraciones myConfiguration;
    //objeto para modificar las variables del entorno leyendo las configuraciones
    ManejoDeConfiguraciones myFManejoDeConfiguraciones;
    
    //Arreglos temporales para mostrar datos
    public double [] datosTiempoReal = new double [2]; //aqui guardamos el dato medido en tiempo real
    public double [][] datosGraficaHistoricos = new double [1000][2]; //aqui guardamos el set de datos del sensor a visualizar
    
    //objeto tipo grafica para la visualizacion de los datos:
    Graficas graficaTiempoReal;
    Graficas graficaHistoricos;
    
    //los que agregan datos a las graficas
    //runnable para dquirir los datos historicos del servidor
    //runable para adquirir los datos en tiempo real de los sensores
    
    //los que modificaran los paneles de 
    ProgresBarRunnable runableBar_consumoElectrico;
    ProgresBarRunnable runableBar_consumoHidrico;
    ProgresBarRunnable runableBar_indiceConfort;
    
    
    
    public interfaz() {
        
        
        initComponents();
        
        //creamos el objeto de configuraciones
        myConfiguration = new Configuraciones();
        
        //creamos el objeto para el manejo del Fondo y actualizamos el fondo:
        myFManejoDeConfiguraciones = new ManejoDeConfiguraciones();
        
        //actualizar los demas datos empezamos por el fondo:
        myFManejoDeConfiguraciones.actualizarTodo();
        //actualizar watt hora lo demas
        //...
        //..
        
        //creamos los actualizadoes de las indicadores:
        runableBar_consumoElectrico = new ProgresBarRunnable(progresBarPanelEnergia,1);
        runableBar_consumoHidrico = new ProgresBarRunnable(progresBarPanelAgua,5);
        runableBar_indiceConfort = new ProgresBarRunnable(progresBarPanelTemperatura,10);
        
        
        //restrinjo los cuadros de texto de configuraciones para que solo reciban numeros con punto decimal
        soloFlotantes(jTextFieldCostoAgua);
        soloFlotantes(jTextFieldCostoWatt);
        soloEnteros(jTextFieldDiaFacturacion);
        soloEnteros(jTextFieldNumPersonas);
        
        
        super.setLocationRelativeTo(null); //esto provoca que al iniciar el jFrame se ubique centrado en la pantalla
    
        
    }
    public void actualizarGuiConfiguraciones(){
        jTextFieldCostoAgua.setText(String.valueOf(interfaz.metrosCubicos));
        jTextFieldCostoWatt.setText(String.valueOf(interfaz.wattHora));
        jTextFieldDiaFacturacion.setText(String.valueOf(interfaz.diaFacturacion));
        jTextFieldNumPersonas.setText(String.valueOf(interfaz.numUsuarios));
        jCheckBoxNotificaciones.setSelected(interfaz.notificaciones);
        
    }
    /*
    public Graficas crearGrafica(String nombreGrafica, String nombreEjeX, String nombreEjeY){
        Graficas pruebaMyObjetoGrafica = new Graficas(nombreGrafica, nombreEjeX, nombreEjeY);
        return pruebaMyObjetoGrafica;
    }
    public void agregarDatos2(double [][] datos, String dataName, Graficas ObjetoGrafica){
        ObjetoGrafica.agregarDatos(dataName, datos);
        ObjetoGrafica.crearLineChart();
    }
    */
    public void soloFlotantes (JTextField a) {
        a.addKeyListener (new KeyAdapter() {
            @Override
            public void keyTyped (KeyEvent e) {
                char c=e.getKeyChar ();
                if (!((Character.isDigit (c))||(c=='.'))) {
                    e.consume();
                }
            }
        });
    }
    public void soloEnteros (JTextField a) {
        a.addKeyListener (new KeyAdapter() {
            @Override
            public void keyTyped (KeyEvent e) {
                char c=e.getKeyChar ();
                if (!(Character.isDigit (c))) {
                    e.consume();
                }
            }
        });
    }
    public void changePanel(javax.swing.JPanel panelActivado){
        javax.swing.JPanel  matrizPaneles [] = new javax.swing.JPanel []{
            Eficiencia,
            Graficas,
            Principal,
            Agenda,
        };
        for(int i=0;i<matrizPaneles.length;i++){
            
            if(panelActivado.equals(matrizPaneles[i])){
                panelActivado.setVisible(true);
                panelActivado.setEnabled(true);
            }else{
                matrizPaneles[i].setVisible(false);
                matrizPaneles[i].setEnabled(false);
            }
        }
    }
    
    public void changePanel(javax.swing.JPanel panelActivado,javax.swing.JPanel panelInternoActivado){
       
        javax.swing.JPanel  matrizPaneles [] = new javax.swing.JPanel []{
            Eficiencia,
            Graficas,
            Principal,
            Agenda,
        };
        javax.swing.JPanel  matrizPanelesInternos [] = new javax.swing.JPanel []{
            jPanelAgua,
            jPanelBasuras,
            jPanelTecnicas,
            PanelHistoricos,
            PanelTiempoReal
        };
            
        for(int i=0;i<matrizPanelesInternos.length;i++){
            
            if(panelInternoActivado.equals(matrizPanelesInternos[i])){
                panelInternoActivado.setVisible(true);
                panelInternoActivado.setEnabled(true);
            }else{
                matrizPanelesInternos[i].setVisible(false);
                matrizPanelesInternos[i].setEnabled(false);
            }
        }
        for(int i=0;i<matrizPaneles.length;i++){
            
            if(panelActivado.equals(matrizPaneles[i])){
                panelActivado.setVisible(true);
                panelActivado.setEnabled(true);
            }else{
                matrizPaneles[i].setVisible(false);
                matrizPaneles[i].setEnabled(false);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        configuraciones = new javax.swing.JDialog();
        atrasConfiguraciones = new ClasesVarias.Botones.CustomBoton();
        menuBarApp4 = new javax.swing.JLabel();
        jPanel_texto = new javax.swing.JPanel();
        costoWat = new javax.swing.JLabel();
        costoAgua = new javax.swing.JLabel();
        numeroPersonas = new javax.swing.JLabel();
        diaDeFacturacion = new javax.swing.JLabel();
        notificacioneCelular = new javax.swing.JLabel();
        seleccionarFondo = new javax.swing.JLabel();
        jPanel_datos = new javax.swing.JPanel();
        panelCustomDesplegableFondo = new ClasesVarias.Botones.PanelCustomDesplegable(new String[]{"Fondo 1","Fondo 2","Fondo 3","Fondo 4"});
        jTextFieldCostoWatt = new javax.swing.JTextField();
        jTextFieldCostoAgua = new javax.swing.JTextField();
        jTextFieldNumPersonas = new javax.swing.JTextField();
        jTextFieldDiaFacturacion = new javax.swing.JTextField();
        jCheckBoxNotificaciones = new javax.swing.JCheckBox();
        textoDefault = new javax.swing.JLabel();
        jlabelDefault = new ClasesVarias.Botones.JlabelBoton();
        textoGuardarConfig1 = new javax.swing.JLabel();
        labelBotonGuardarConfig = new ClasesVarias.Botones.JlabelBoton();
        fondoParasito = new javax.swing.JLabel();
        Principal = new javax.swing.JPanel();
        salir = new ClasesVarias.Botones.CustomBoton();
        config = new ClasesVarias.Botones.CustomBoton();
        usuarios = new ClasesVarias.Botones.CustomBoton();
        menuBar = new javax.swing.JLabel();
        Aplicaciones = new javax.swing.JPanel();
        appGraficas = new ClasesVarias.Botones.CustomBoton();
        appHorarios = new ClasesVarias.Botones.CustomBoton();
        appGestion = new ClasesVarias.Botones.CustomBoton();
        panelAplicaciones = new javax.swing.JLabel();
        tituloAplicaciones = new javax.swing.JLabel();
        Mensajes = new javax.swing.JPanel();
        panelMensajes = new javax.swing.JLabel();
        tituloMensajes = new javax.swing.JLabel();
        progresBarPanelEnergia = new ClasesVarias.PanelDeProgreso.ProgresBarPanel();
        progresBarPanelAgua = new ClasesVarias.PanelDeProgreso.ProgresBarPanel();
        progresBarPanelTemperatura = new ClasesVarias.PanelDeProgreso.ProgresBarPanel();
        Graficas = new javax.swing.JPanel();
        atrasGraficas = new ClasesVarias.Botones.CustomBoton();
        menuBarApp2 = new javax.swing.JLabel();
        historicos = new ClasesVarias.Botones.JlabelBoton("/fondos/graficasHistoricas.png","/fondos/graficasHistoricas.png","/fondos/graficasHistoricas.png");
        Reales = new ClasesVarias.Botones.JlabelBoton("/fondos/graficasReal.png","/fondos/graficasReal.png","/fondos/graficasReal.png");
        panel = new javax.swing.JPanel();
        PanelTiempoReal = new javax.swing.JPanel();
        PanelHistoricos = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        panelCustomDesplegable_variable = new ClasesVarias.Botones.PanelCustomDesplegable(new String[] { "voltaje pico AC", "corriente pico AC", "voltaje DC", "corriente DC","consumo de agua", "temepratura", "humedad"});
        panelCustomDesplegable_periodo = new ClasesVarias.Botones.PanelCustomDesplegable(new String[] { "una hora", "un dia", "una semana", "un mes" });
        periodoMostrado = new javax.swing.JLabel();
        variableMostrada = new javax.swing.JLabel();
        actualizarGrafica = new ClasesVarias.Botones.JlabelBoton();
        periodoMostrado1 = new javax.swing.JLabel();
        Eficiencia = new javax.swing.JPanel();
        atrasEficiencia = new ClasesVarias.Botones.CustomBoton();
        menuBarApp1 = new javax.swing.JLabel();
        jPanelAgua = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jCheckBox39 = new javax.swing.JCheckBox();
        jCheckBox9 = new javax.swing.JCheckBox();
        jCheckBox41 = new javax.swing.JCheckBox();
        jCheckBox16 = new javax.swing.JCheckBox();
        jCheckBox40 = new javax.swing.JCheckBox();
        jCheckBox42 = new javax.swing.JCheckBox();
        jCheckBox15 = new javax.swing.JCheckBox();
        jCheckBox43 = new javax.swing.JCheckBox();
        jCheckBox8 = new javax.swing.JCheckBox();
        jCheckBox12 = new javax.swing.JCheckBox();
        jPanelBasuras = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jCheckBox17 = new javax.swing.JCheckBox();
        jCheckBox18 = new javax.swing.JCheckBox();
        jCheckBox24 = new javax.swing.JCheckBox();
        jCheckBox25 = new javax.swing.JCheckBox();
        jCheckBox26 = new javax.swing.JCheckBox();
        jCheckBox27 = new javax.swing.JCheckBox();
        jCheckBox28 = new javax.swing.JCheckBox();
        jCheckBox44 = new javax.swing.JCheckBox();
        jCheckBox19 = new javax.swing.JCheckBox();
        jCheckBox20 = new javax.swing.JCheckBox();
        jCheckBox21 = new javax.swing.JCheckBox();
        jCheckBox22 = new javax.swing.JCheckBox();
        jCheckBox23 = new javax.swing.JCheckBox();
        jPanelTecnicas = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jCheckBox45 = new javax.swing.JCheckBox();
        jCheckBox46 = new javax.swing.JCheckBox();
        jCheckBox48 = new javax.swing.JCheckBox();
        jCheckBox47 = new javax.swing.JCheckBox();
        jCheckBox49 = new javax.swing.JCheckBox();
        jCheckBox50 = new javax.swing.JCheckBox();
        jCheckBox51 = new javax.swing.JCheckBox();
        jCheckBox52 = new javax.swing.JCheckBox();
        jCheckBox55 = new javax.swing.JCheckBox();
        jCheckBox54 = new javax.swing.JCheckBox();
        jCheckBox53 = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        pestañaCumplimiento = new javax.swing.JLabel();
        pestañaBasuras = new javax.swing.JLabel();
        pestañaAguas = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        guardarDatos = new ClasesVarias.Botones.CustomBoton("/botones/boton1N.png","/botones/boton1E.png","/botones/boton1P.png");
        Agenda = new javax.swing.JPanel();
        atrasAgenda = new ClasesVarias.Botones.CustomBoton();
        menuBarApp3 = new javax.swing.JLabel();
        labelActivarPermiso = new javax.swing.JLabel();
        labelGuardarHorario = new javax.swing.JLabel();
        panelCustomDesplegable_circuito = new ClasesVarias.Botones.PanelCustomDesplegable(new String [] {
            "estufa", "Refrigeracion", "lavadora", "Sala"
        });
        pedirPermiso = new ClasesVarias.Botones.JlabelBoton();
        guardarHorario = new ClasesVarias.Botones.JlabelBoton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        panelAgenda = new javax.swing.JLabel();
        pestañaAgenda = new javax.swing.JLabel();
        fondo = new javax.swing.JLabel();

        configuraciones.setUndecorated(true);
        configuraciones.setSize(new java.awt.Dimension(397, 440));
        configuraciones.setLocationRelativeTo(null);
        //configuraciones.setOpacity(0.5);
        //configuraciones.setLocation(configuraciones.getLocation().x-configuraciones.getWidth()/2,configuraciones.getLocation().y-configuraciones.getHeight()/2);
        configuraciones.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        atrasConfiguraciones.setIcons("/botones/backN.png", "/botones/backE.png", "/botones/backP.png");
        atrasConfiguraciones.setText("");
        atrasConfiguraciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasConfiguracionesActionPerformed(evt);
            }
        });
        configuraciones.getContentPane().add(atrasConfiguraciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 60, 40));

        menuBarApp4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/menu2.png"))); // NOI18N
        configuraciones.getContentPane().add(menuBarApp4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, -1));

        jPanel_texto.setOpaque(false);

        costoWat.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        costoWat.setForeground(new java.awt.Color(255, 255, 255));
        costoWat.setText("Costo watt/h");
        costoWat.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        costoAgua.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        costoAgua.setForeground(new java.awt.Color(255, 255, 255));
        costoAgua.setText("Costo m^3 de agua");
        costoAgua.setFocusable(false);
        costoAgua.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        numeroPersonas.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        numeroPersonas.setForeground(new java.awt.Color(255, 255, 255));
        numeroPersonas.setText("Personas en el hogar");
        numeroPersonas.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        diaDeFacturacion.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        diaDeFacturacion.setForeground(new java.awt.Color(255, 255, 255));
        diaDeFacturacion.setText("Dia de facturacion");
        diaDeFacturacion.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        notificacioneCelular.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        notificacioneCelular.setForeground(new java.awt.Color(255, 255, 255));
        notificacioneCelular.setText("Notificaciones al celular");
        notificacioneCelular.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        seleccionarFondo.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        seleccionarFondo.setForeground(new java.awt.Color(255, 255, 255));
        seleccionarFondo.setText("Seleccionar Fondo");
        seleccionarFondo.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel_textoLayout = new javax.swing.GroupLayout(jPanel_texto);
        jPanel_texto.setLayout(jPanel_textoLayout);
        jPanel_textoLayout.setHorizontalGroup(
            jPanel_textoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_textoLayout.createSequentialGroup()
                .addContainerGap(39, Short.MAX_VALUE)
                .addGroup(jPanel_textoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(numeroPersonas, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(costoWat, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(costoAgua, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(diaDeFacturacion, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(notificacioneCelular, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(seleccionarFondo, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel_textoLayout.setVerticalGroup(
            jPanel_textoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_textoLayout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(seleccionarFondo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(costoWat, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(costoAgua, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(numeroPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(diaDeFacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(notificacioneCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );

        configuraciones.getContentPane().add(jPanel_texto, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 190, 280));

        jPanel_datos.setOpaque(false);
        jPanel_datos.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel_datos.add(panelCustomDesplegableFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, 170));

        jTextFieldCostoWatt.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jTextFieldCostoWatt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCostoWatt.setName("costo del watt hora"); // NOI18N
        jPanel_datos.add(jTextFieldCostoWatt, new org.netbeans.lib.awtextra.AbsoluteConstraints(24, 62, 83, -1));

        jTextFieldCostoAgua.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jTextFieldCostoAgua.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCostoAgua.setName("costo del metro cubico del agua"); // NOI18N
        jPanel_datos.add(jTextFieldCostoAgua, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 83, -1));

        jTextFieldNumPersonas.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jTextFieldNumPersonas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldNumPersonas.setName("numero de personas en el hogar"); // NOI18N
        jPanel_datos.add(jTextFieldNumPersonas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 83, -1));

        jTextFieldDiaFacturacion.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jTextFieldDiaFacturacion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldDiaFacturacion.setName("dia de facturacion"); // NOI18N
        jPanel_datos.add(jTextFieldDiaFacturacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 83, -1));
        jPanel_datos.add(jCheckBoxNotificaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, -1, -1));

        configuraciones.getContentPane().add(jPanel_datos, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 60, 210, 280));

        textoDefault.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        textoDefault.setForeground(new java.awt.Color(255, 255, 255));
        textoDefault.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textoDefault.setText("Volver al default");
        textoDefault.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        configuraciones.getContentPane().add(textoDefault, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, 120, 20));

        jlabelDefault.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlabelDefaultMouseClicked(evt);
            }
        });
        configuraciones.getContentPane().add(jlabelDefault, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 380, -1, -1));

        textoGuardarConfig1.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        textoGuardarConfig1.setForeground(new java.awt.Color(255, 255, 255));
        textoGuardarConfig1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textoGuardarConfig1.setText("Guardar Todo");
        textoGuardarConfig1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        configuraciones.getContentPane().add(textoGuardarConfig1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 390, 130, 20));

        labelBotonGuardarConfig.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelBotonGuardarConfigMouseClicked(evt);
            }
        });
        configuraciones.getContentPane().add(labelBotonGuardarConfig, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 380, -1, -1));
        configuraciones.getContentPane().add(fondoParasito, new org.netbeans.lib.awtextra.AbsoluteConstraints(-3, -3, 410, 450));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("interfazManager");
        setMaximumSize(new java.awt.Dimension(800, 480));
        setMinimumSize(new java.awt.Dimension(800, 480));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Principal.setMaximumSize(new java.awt.Dimension(800, 480));
        Principal.setMinimumSize(new java.awt.Dimension(800, 480));
        Principal.setOpaque(false);
        Principal.setPreferredSize(new java.awt.Dimension(800, 480));
        Principal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        salir.setIcons("/botones/salirN.png", "/botones/salirE.png", "/botones/salirP.png");
        salir.setText("");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        Principal.add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 0, 40, 40));

        config.setIcons("/botones/configN.png", "/botones/configE.png", "/botones/configP.png");
        config.setText("customBoton1");
        config.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configActionPerformed(evt);
            }
        });
        Principal.add(config, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 0, 40, 40));

        usuarios.setIcons("/botones/usuarioN.png", "/botones/usuarioE.png", "/botones/usuarioP.png");
        usuarios.setText("");
        usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usuariosActionPerformed(evt);
            }
        });
        Principal.add(usuarios, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 0, 40, 40));

        menuBar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/menuPrincipal.png"))); // NOI18N
        Principal.add(menuBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 0, -1, -1));

        Aplicaciones.setMaximumSize(new java.awt.Dimension(383, 388));
        Aplicaciones.setMinimumSize(new java.awt.Dimension(383, 388));
        Aplicaciones.setOpaque(false);
        Aplicaciones.setPreferredSize(new java.awt.Dimension(383, 388));
        Aplicaciones.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        appGraficas.setIcons("/botones/appGraficaN.png", "/botones/appGraficaE.png", "/botones/appGraficaP.png");
        appGraficas.setText("");
        appGraficas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                appGraficasActionPerformed(evt);
            }
        });
        Aplicaciones.add(appGraficas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 60, 50));

        appHorarios.setIcons("/botones/appGestionN.png","/botones/appGestionE.png","/botones/appGestionP.png");
        appHorarios.setText("");
        appHorarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                appHorariosActionPerformed(evt);
            }
        });
        Aplicaciones.add(appHorarios, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 60, 50));

        appGestion.setIcons("/botones/appEficienciaN.png","/botones/appEficienciaE.png","/botones/appEficienciaP.png");
        appGestion.setText("");
        appGestion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                appGestionActionPerformed(evt);
            }
        });
        Aplicaciones.add(appGestion, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 60, 60, 50));

        panelAplicaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/panelApp.png"))); // NOI18N
        Aplicaciones.add(panelAplicaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, -1, 350));

        tituloAplicaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/pestañaApp.png"))); // NOI18N
        Aplicaciones.add(tituloAplicaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, -1));

        Principal.add(Aplicaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, 383, 388));

        Mensajes.setMaximumSize(new java.awt.Dimension(267, 154));
        Mensajes.setMinimumSize(new java.awt.Dimension(267, 154));
        Mensajes.setOpaque(false);
        Mensajes.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panelMensajes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/panelMensajes.png"))); // NOI18N
        Mensajes.add(panelMensajes, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, -1, -1));

        tituloMensajes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/pestañaMensajes.png"))); // NOI18N
        Mensajes.add(tituloMensajes, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 180, -1));

        Principal.add(Mensajes, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 100, 267, 154));

        progresBarPanelEnergia.setText("Energia");

        javax.swing.GroupLayout progresBarPanelEnergiaLayout = new javax.swing.GroupLayout(progresBarPanelEnergia);
        progresBarPanelEnergia.setLayout(progresBarPanelEnergiaLayout);
        progresBarPanelEnergiaLayout.setHorizontalGroup(
            progresBarPanelEnergiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        progresBarPanelEnergiaLayout.setVerticalGroup(
            progresBarPanelEnergiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        Principal.add(progresBarPanelEnergia, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 330, 130, 100));

        progresBarPanelAgua.setText("Agua");

        Principal.add(progresBarPanelAgua, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 330, 130, 100));

        progresBarPanelTemperatura.setText("Temperatura");

        Principal.add(progresBarPanelTemperatura, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 330, 130, 100));

        setSize(800,480);

        getContentPane().add(Principal, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 480));

        Graficas.setMaximumSize(new java.awt.Dimension(800, 480));
        Graficas.setOpaque(false);
        Graficas.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        atrasGraficas.setIcons("/botones/backN.png", "/botones/backE.png", "/botones/backP.png");
        atrasGraficas.setText("");
        atrasGraficas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasGraficasActionPerformed(evt);
            }
        });
        Graficas.add(atrasGraficas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 60, 40));

        menuBarApp2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/menu2.png"))); // NOI18N
        Graficas.add(menuBarApp2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 730, -1));

        historicos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                historicosMouseClicked(evt);
            }
        });
        Graficas.add(historicos, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 220, -1));

        Reales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RealesMouseClicked(evt);
            }
        });
        Graficas.add(Reales, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 60, 230, -1));

        panel.setOpaque(false);
        panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PanelTiempoReal.setMaximumSize(new java.awt.Dimension(410, 300));
        PanelTiempoReal.setMinimumSize(new java.awt.Dimension(410, 300));
        PanelTiempoReal.setOpaque(false);
        PanelTiempoReal.setLayout(new java.awt.BorderLayout());
        panel.add(PanelTiempoReal, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 410, 300));

        PanelHistoricos.setMaximumSize(new java.awt.Dimension(410, 300));
        PanelHistoricos.setMinimumSize(new java.awt.Dimension(410, 300));

        javax.swing.GroupLayout PanelHistoricosLayout = new javax.swing.GroupLayout(PanelHistoricos);
        PanelHistoricos.setLayout(PanelHistoricosLayout);
        PanelHistoricosLayout.setHorizontalGroup(
            PanelHistoricosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 410, Short.MAX_VALUE)
        );
        PanelHistoricosLayout.setVerticalGroup(
            PanelHistoricosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        panel.add(PanelHistoricos, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 410, 300));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/panelGraficas.png"))); // NOI18N
        panel.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        Graficas.add(panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 470, 350));
        Graficas.add(panelCustomDesplegable_variable, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 110, -1, 290));
        Graficas.add(panelCustomDesplegable_periodo, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 190, -1, 240));

        periodoMostrado.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        periodoMostrado.setForeground(new java.awt.Color(255, 255, 255));
        periodoMostrado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        periodoMostrado.setText("Actualizar Gráfica");
        Graficas.add(periodoMostrado, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 320, 120, 20));

        variableMostrada.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        variableMostrada.setForeground(new java.awt.Color(255, 255, 255));
        variableMostrada.setText("Variable Medida:");
        Graficas.add(variableMostrada, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 120, -1, 20));

        actualizarGrafica.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                actualizarGraficaMouseClicked(evt);
            }
        });
        Graficas.add(actualizarGrafica, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 310, -1, -1));

        periodoMostrado1.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        periodoMostrado1.setForeground(new java.awt.Color(255, 255, 255));
        periodoMostrado1.setText("Periodo de Medida:");
        Graficas.add(periodoMostrado1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 200, -1, 20));

        setSize(800,480);

        getContentPane().add(Graficas, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 480));

        Eficiencia.setMaximumSize(new java.awt.Dimension(800, 480));
        Eficiencia.setMinimumSize(new java.awt.Dimension(800, 480));
        Eficiencia.setOpaque(false);
        Eficiencia.setPreferredSize(new java.awt.Dimension(800, 480));
        Eficiencia.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        atrasEficiencia.setIcons("/botones/backN.png", "/botones/backE.png", "/botones/backP.png");
        atrasEficiencia.setText("");
        atrasEficiencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasEficienciaActionPerformed(evt);
            }
        });
        Eficiencia.add(atrasEficiencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 60, 40));

        menuBarApp1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/menu2.png"))); // NOI18N
        Eficiencia.add(menuBarApp1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 730, -1));

        jPanelAgua.setMaximumSize(new java.awt.Dimension(800, 350));
        jPanelAgua.setOpaque(false);
        jPanelAgua.setPreferredSize(new java.awt.Dimension(800, 213));

        jPanel5.setOpaque(false);
        jPanel5.setPreferredSize(new java.awt.Dimension(365, 183));

        jLabel23.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("¿Cierra la llave cuando se cepilla los dientes?");

        jLabel24.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("¿Cierra la llave cuando se enjabona?");

        jLabel22.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("¿Cuanto tiempo demora bañandose?");

        jLabel25.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("¿Reutiliza el agua de la lavadora?");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 95, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel25, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(jLabel24)
                .addGap(27, 27, 27)
                .addComponent(jLabel23)
                .addGap(29, 29, 29)
                .addComponent(jLabel25)
                .addContainerGap())
        );

        jPanel6.setOpaque(false);
        jPanel6.setPreferredSize(new java.awt.Dimension(365, 183));

        jCheckBox39.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox39.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox39.setText("No");

        jCheckBox9.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox9.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox9.setText("Entre 5 y 10 minutos");
        jCheckBox9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox9ActionPerformed(evt);
            }
        });

        jCheckBox41.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox41.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox41.setText("Si");

        jCheckBox16.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox16.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox16.setText("Entre 5 y 10 minutos");

        jCheckBox40.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox40.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox40.setText("Si");

        jCheckBox42.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox42.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox42.setText("No");

        jCheckBox15.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox15.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox15.setText("No");
        jCheckBox15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox15jCheckBox10ActionPerformed(evt);
            }
        });

        jCheckBox43.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox43.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox43.setText("No tengo lavadora");

        jCheckBox8.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox8.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox8.setText("Menos de 5 minutos");

        jCheckBox12.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox12.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox12.setText("Si");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox16)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jCheckBox8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox9))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jCheckBox12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jCheckBox40, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jCheckBox41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jCheckBox42)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBox43))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jCheckBox15))
                            .addComponent(jCheckBox39))))
                .addContainerGap(65, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBox9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox16)
                .addGap(15, 15, 15)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox12)
                    .addComponent(jCheckBox15))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox40)
                    .addComponent(jCheckBox39))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox41)
                    .addComponent(jCheckBox42)
                    .addComponent(jCheckBox43))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelAguaLayout = new javax.swing.GroupLayout(jPanelAgua);
        jPanelAgua.setLayout(jPanelAguaLayout);
        jPanelAguaLayout.setHorizontalGroup(
            jPanelAguaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAguaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanelAguaLayout.setVerticalGroup(
            jPanelAguaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAguaLayout.createSequentialGroup()
                .addGroup(jPanelAguaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        Eficiencia.add(jPanelAgua, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, -1, -1));
        Eficiencia.setOpaque(false); //hacemos opaco el panel

        jPanelBasuras.setMaximumSize(new java.awt.Dimension(800, 480));
        jPanelBasuras.setOpaque(false);
        jPanelBasuras.setPreferredSize(new java.awt.Dimension(800, 296));

        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(365, 266));

        jLabel26.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("¿Clasifica la basura que hay en su casa?");

        jLabel28.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Indique que contenedores de basura tiene en su casa:");

        jLabel27.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setText("¿Que tipo de basura clasifica?");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel26)
                .addGap(31, 31, 31)
                .addComponent(jLabel28)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 158, Short.MAX_VALUE)
                .addComponent(jLabel27)
                .addGap(29, 29, 29))
        );

        jPanel2.setOpaque(false);
        jPanel2.setPreferredSize(new java.awt.Dimension(365, 266));

        jCheckBox17.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox17.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox17.setText("Si");

        jCheckBox18.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox18.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox18.setText("No");

        jCheckBox24.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox24.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox24.setText("Gris (desechos varios)");

        jCheckBox25.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox25.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox25.setText("Naranja (basura organica)");

        jCheckBox26.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox26.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox26.setText("Verde (Envases de vidrio)");

        jCheckBox27.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox27.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox27.setText("Amarillo (plastico y envases metalicos)");

        jCheckBox28.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox28.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox28.setText("Azul (papel y carton)");

        jCheckBox44.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox44.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox44.setText("Rojol (desechos infecciosos)");

        jCheckBox19.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox19.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox19.setText("Vidrio");
        jCheckBox19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox19jCheckBox18ActionPerformed(evt);
            }
        });

        jCheckBox20.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox20.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox20.setText("Medicamentos");
        jCheckBox20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox20jCheckBox19ActionPerformed(evt);
            }
        });

        jCheckBox21.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox21.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox21.setText("Papel y carton");
        jCheckBox21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox21jCheckBox20ActionPerformed(evt);
            }
        });

        jCheckBox22.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox22.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox22.setText("Materia organica");
        jCheckBox22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox22jCheckBox21ActionPerformed(evt);
            }
        });

        jCheckBox23.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox23.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox23.setText("Plastico y metal");
        jCheckBox23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox23jCheckBox22ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jCheckBox22)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox23))
                    .addComponent(jCheckBox44)
                    .addComponent(jCheckBox28)
                    .addComponent(jCheckBox27)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jCheckBox19)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox20)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox21))
                    .addComponent(jCheckBox26)
                    .addComponent(jCheckBox25)
                    .addComponent(jCheckBox24)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jCheckBox17)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox18)))
                .addContainerGap(48, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox17)
                    .addComponent(jCheckBox18))
                .addGap(18, 18, 18)
                .addComponent(jCheckBox24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox26, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox28)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox19)
                    .addComponent(jCheckBox20)
                    .addComponent(jCheckBox21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox22)
                    .addComponent(jCheckBox23)))
        );

        javax.swing.GroupLayout jPanelBasurasLayout = new javax.swing.GroupLayout(jPanelBasuras);
        jPanelBasuras.setLayout(jPanelBasurasLayout);
        jPanelBasurasLayout.setHorizontalGroup(
            jPanelBasurasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBasurasLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelBasurasLayout.setVerticalGroup(
            jPanelBasurasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBasurasLayout.createSequentialGroup()
                .addGroup(jPanelBasurasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        Eficiencia.add(jPanelBasuras, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, -1, -1));
        Eficiencia.setVisible(false);
        Eficiencia.setEnabled(false);

        jPanelTecnicas.setMaximumSize(new java.awt.Dimension(800, 179));
        jPanelTecnicas.setOpaque(false);
        jPanelTecnicas.setPreferredSize(new java.awt.Dimension(800, 209));

        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(365, 179));

        jLabel31.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setText(" ¿Apaga los electrodomesticos cuando no los utiliza?");

        jLabel29.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setText("¿Que bombillas utiliza?");

        jLabel30.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("¿Su nevera cuenta con la estampilla de eficiencia RETIQ?");

        jLabel32.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setText("¿Su lavadora cuenta con la estampilla de eficiencia RETIQ?");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel30, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel29)
                .addGap(27, 27, 27)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jLabel30)
                .addGap(30, 30, 30)
                .addComponent(jLabel32)
                .addGap(31, 31, 31))
        );

        jPanel4.setOpaque(false);
        jPanel4.setPreferredSize(new java.awt.Dimension(365, 179));

        jCheckBox45.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox45.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox45.setText("Incandesente");

        jCheckBox46.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox46.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox46.setText("Led");

        jCheckBox48.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox48.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox48.setText("Fluoresente");

        jCheckBox47.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox47.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox47.setText("Si");
        jCheckBox47.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox47ActionPerformed(evt);
            }
        });

        jCheckBox49.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox49.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox49.setText("No");

        jCheckBox50.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox50.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox50.setText("Si");

        jCheckBox51.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox51.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox51.setText("No");

        jCheckBox52.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox52.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox52.setText("No tiene nevera");

        jCheckBox55.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox55.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox55.setText("No tiene nevera");

        jCheckBox54.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox54.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox54.setText("No");

        jCheckBox53.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jCheckBox53.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox53.setText("Si");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jCheckBox53)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox54)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox55))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jCheckBox45)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox46)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox48))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jCheckBox47)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBox49))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jCheckBox50)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBox51)))
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox52)))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox45)
                    .addComponent(jCheckBox46)
                    .addComponent(jCheckBox48))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox49)
                    .addComponent(jCheckBox47))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox50)
                    .addComponent(jCheckBox51)
                    .addComponent(jCheckBox52))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox53)
                    .addComponent(jCheckBox54)
                    .addComponent(jCheckBox55))
                .addGap(27, 27, 27))
        );

        javax.swing.GroupLayout jPanelTecnicasLayout = new javax.swing.GroupLayout(jPanelTecnicas);
        jPanelTecnicas.setLayout(jPanelTecnicasLayout);
        jPanelTecnicasLayout.setHorizontalGroup(
            jPanelTecnicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTecnicasLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanelTecnicasLayout.setVerticalGroup(
            jPanelTecnicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTecnicasLayout.createSequentialGroup()
                .addGroup(jPanelTecnicasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 30, Short.MAX_VALUE))
        );

        Eficiencia.add(jPanelTecnicas, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, -1, -1));
        Eficiencia.setVisible(false);
        Eficiencia.setEnabled(false);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/panelEficiencia.png"))); // NOI18N
        Eficiencia.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 740, -1));

        pestañaCumplimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/cumplimientoCentro.png"))); // NOI18N
        pestañaCumplimiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pestañaCumplimientoMouseClicked(evt);
            }
        });
        Eficiencia.add(pestañaCumplimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, 260, 40));

        pestañaBasuras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoBasurasCentro.png"))); // NOI18N
        pestañaBasuras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pestañaBasurasMouseClicked(evt);
            }
        });
        Eficiencia.add(pestañaBasuras, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, 240, 40));

        pestañaAguas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoAguasBorde.png"))); // NOI18N
        pestañaAguas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pestañaAguasMouseClicked(evt);
            }
        });
        Eficiencia.add(pestañaAguas, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 70, -1, 40));

        jLabel2.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Guardar todo");
        jLabel2.setEnabled(false);
        Eficiencia.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 440, 80, -1));

        guardarDatos.setText("");
        guardarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarDatosActionPerformed(evt);
            }
        });
        Eficiencia.add(guardarDatos, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 430, 130, 40));

        setSize(800,480);

        getContentPane().add(Eficiencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        Agenda.setMaximumSize(new java.awt.Dimension(800, 480));
        Agenda.setMinimumSize(new java.awt.Dimension(800, 480));
        Agenda.setOpaque(false);
        Agenda.setPreferredSize(new java.awt.Dimension(800, 480));
        Agenda.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        atrasAgenda.setIcons("/botones/backN.png", "/botones/backE.png", "/botones/backP.png");
        atrasAgenda.setText("");
        atrasAgenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasAgendaActionPerformed(evt);
            }
        });
        Agenda.add(atrasAgenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 60, 40));

        menuBarApp3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/menu2.png"))); // NOI18N
        Agenda.add(menuBarApp3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 730, -1));

        labelActivarPermiso.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        labelActivarPermiso.setForeground(new java.awt.Color(255, 255, 255));
        labelActivarPermiso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelActivarPermiso.setText("Activar por 1 hora:");
        Agenda.add(labelActivarPermiso, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 140, 120, 20));

        labelGuardarHorario.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        labelGuardarHorario.setForeground(new java.awt.Color(255, 255, 255));
        labelGuardarHorario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelGuardarHorario.setText("Guardar Horario");
        Agenda.add(labelGuardarHorario, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 370, 130, 20));
        Agenda.add(panelCustomDesplegable_circuito, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 180, -1, 180));

        pedirPermiso.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pedirPermisoMouseClicked(evt);
            }
        });
        Agenda.add(pedirPermiso, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 130, -1, -1));

        guardarHorario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                guardarHorarioMouseClicked(evt);
            }
        });
        Agenda.add(guardarHorario, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 360, -1, -1));

        jPanel7.setOpaque(false);
        jPanel7.setLayout(new java.awt.CardLayout());

        jScrollPane1.setOpaque(false);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {1, null, null, null, null},
                {2, null, null, null, null},
                {3, null, null, null, null},
                {4, null, null, null, null},
                {5, on, null, null, null},
                {6, null, null, null, null},
                {7, null, on, null, null},
                {8, null, on, on, null},
                {9, null, on, on, null},
                {10, null, on, null, null},
                {11, on, on, null, null},
                {12, on, on, null, null},
                {13, null, on, null, null},
                {14, null, on, null, null},
                {15, null, on, null, on},
                {16, null, on, null, on},
                {17, null, on, null, on},
                {18, null, on, null, on},
                {19, on, null, null, on},
                {20, null, null, null, on},
                {21, null, null, null, null},
                {22, null, null, null, null},
                {23, null, null, null, null},
                {24, null, null, null, null},
            },
            new String [] {
                "hora del dia", "estufa", "Refrigeracion", "lavadora", "Sala"
            }
        ));
        jTable1.setOpaque(false);
        jScrollPane1.setViewportView(jTable1);

        jPanel7.add(jScrollPane1, "card2");

        Agenda.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 470, 330));

        panelAgenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/panelHorario.png"))); // NOI18N
        Agenda.add(panelAgenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        pestañaAgenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/pestañaHorario.png"))); // NOI18N
        Agenda.add(pestañaAgenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 260, 40));

        setSize(800,480);

        getContentPane().add(Agenda, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 480));

        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo2.png"))); // NOI18N
        fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo2.png"))); // NOI18N
        imagenFondo=new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo2.png"));
        fondo.setFocusable(false);
        getContentPane().add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salirActionPerformed

    private void configActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configActionPerformed
        
        configuraciones.setVisible(true);
        configuraciones.setEnabled(true);
        actualizarGuiConfiguraciones();
        
    }//GEN-LAST:event_configActionPerformed

    private void appHorariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_appHorariosActionPerformed
        changePanel(Agenda);
    }//GEN-LAST:event_appHorariosActionPerformed

    private void appGestionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_appGestionActionPerformed
        changePanel(Eficiencia,jPanelAgua);
    }//GEN-LAST:event_appGestionActionPerformed

    private void appGraficasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_appGraficasActionPerformed
        changePanel(Graficas);
    }//GEN-LAST:event_appGraficasActionPerformed

    private void atrasEficienciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasEficienciaActionPerformed
        changePanel(Principal);
    }//GEN-LAST:event_atrasEficienciaActionPerformed

    private void atrasGraficasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasGraficasActionPerformed
        changePanel(Principal);
    }//GEN-LAST:event_atrasGraficasActionPerformed

    private void atrasAgendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasAgendaActionPerformed
        changePanel(Principal);
    }//GEN-LAST:event_atrasAgendaActionPerformed

    private void usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usuariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usuariosActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
        //mostramos el panel principal
        changePanel(Principal);
        
        //añadimos el fondo a los dialogos del menu
        fondoParasito.setIcon(imagenFondo);
        
        //iniciamos los hilos tipo para actualizar
        Thread tb1 = new Thread(runableBar_consumoElectrico);
        Thread tb2 = new Thread(runableBar_consumoHidrico);
        Thread tb3 = new Thread(runableBar_indiceConfort);
        
        tb1.start();
        tb2.start();
        tb3.start();
        
        
        datosGraficaHistoricos=funcionesVarias.rangoSeno(0, 100, 0.1);
        
        //creamos las graficas en cuestion
        graficaHistoricos =  new Graficas("Grafica Datos Historicos", "eje x", "eje y");
        //crearGrafica("Grafica Datos Historicos", "eje x", "eje y");
        
        //Agregamos los datos a la grafica
        graficaHistoricos.agregarDatos("datos1", datosGraficaHistoricos);
        
        //añadimos el grafico lineal
        graficaHistoricos.crearLineChart();
        
        //añadimos la grafica a un panel
        graficaHistoricos.agregarAPanel(PanelHistoricos);
        
        //configuramos la grafica
        /*
        graficaTemperatura.setBackgroundPaint(new Color(54,63,73));
        graficaTemperatura.setBorderVisible(false);
        graficaTemperatura.setBorderPaint(new Color(34,33,73));
        graficaTemperatura.getXYPlot().setBackgroundPaint(new Color(34,33,73));
        graficaTemperatura.getXYPlot().setOutlinePaint(new Color(54,33,173));
        */
        
        //graficaHistoricos.grafica.setBackgroundPaint(new Color(12,12,12));
        //graficaHistoricos.grafica.getXYPlot().setOutlinePaint(new Color(0,146,69));
        //graficaHistoricos.grafica.setBorderVisible(false);
        
        
        //graficaHistoricos.agregarAPanel(PanelHistoricos); 
        
        
                
        
        
    }//GEN-LAST:event_formWindowOpened

    private void atrasConfiguracionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasConfiguracionesActionPerformed
        configuraciones.setEnabled(false);
        configuraciones.setVisible(false);
        
    }//GEN-LAST:event_atrasConfiguracionesActionPerformed

    private void jCheckBox15jCheckBox10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox15jCheckBox10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox15jCheckBox10ActionPerformed

    private void jCheckBox19jCheckBox18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox19jCheckBox18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox19jCheckBox18ActionPerformed

    private void jCheckBox20jCheckBox19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox20jCheckBox19ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox20jCheckBox19ActionPerformed

    private void jCheckBox21jCheckBox20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox21jCheckBox20ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox21jCheckBox20ActionPerformed

    private void jCheckBox22jCheckBox21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox22jCheckBox21ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox22jCheckBox21ActionPerformed

    private void jCheckBox23jCheckBox22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox23jCheckBox22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox23jCheckBox22ActionPerformed

    private void jCheckBox47ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox47ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox47ActionPerformed

    private void jCheckBox9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox9ActionPerformed

    private void pestañaAguasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pestañaAguasMouseClicked
        //Organizo el Seleccionado
        if(pestañaAguas.getLocation().x!=530){
            Eficiencia.remove(pestañaBasuras);
            Eficiencia.remove(pestañaBasuras);
            Eficiencia.remove(pestañaBasuras);
            
            Eficiencia.add(pestañaAguas, new org.netbeans.lib.awtextra.AbsoluteConstraints(530,70, 260, 40));
            Eficiencia.add(pestañaBasuras, new org.netbeans.lib.awtextra.AbsoluteConstraints(290,70, 260, 40));
            Eficiencia.add(pestañaCumplimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, 260, 40));
            
            pestañaAguas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoAguasBorde.png")));
            pestañaBasuras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoBasurasCentro.png")));
            pestañaCumplimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/cumplimientoCentro.png")));
            
            Eficiencia.validate();
        }
        
        changePanel(Eficiencia, jPanelAgua);
        
    }//GEN-LAST:event_pestañaAguasMouseClicked

    private void pestañaBasurasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pestañaBasurasMouseClicked
        //Organizo el Seleccionado
        if(pestañaBasuras.getLocation().x!=530){
            Eficiencia.remove(pestañaBasuras);
            Eficiencia.remove(pestañaBasuras);
            Eficiencia.remove(pestañaBasuras);
            
            Eficiencia.add(pestañaBasuras, new org.netbeans.lib.awtextra.AbsoluteConstraints(530,70, 260, 40));
            Eficiencia.add(pestañaCumplimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, 260, 40));
            Eficiencia.add(pestañaAguas, new org.netbeans.lib.awtextra.AbsoluteConstraints(60,70, 260, 40));
            
            pestañaBasuras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoBasurasBorde.png")));
            
            pestañaCumplimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/cumplimientoCentro.png")));
            pestañaAguas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoAguasCentro.png")));
            
            Eficiencia.validate();
        }
        changePanel(Eficiencia, jPanelBasuras);
    }//GEN-LAST:event_pestañaBasurasMouseClicked

    private void pestañaCumplimientoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pestañaCumplimientoMouseClicked
        //Organizo el Seleccionado
        if(pestañaCumplimiento.getLocation().x!=530){
            Eficiencia.remove(pestañaBasuras);
            Eficiencia.remove(pestañaBasuras);
            Eficiencia.remove(pestañaBasuras);
            
            Eficiencia.add(pestañaCumplimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 70, 260, 40));
            Eficiencia.add(pestañaAguas, new org.netbeans.lib.awtextra.AbsoluteConstraints(290,70, 260, 40));
            Eficiencia.add(pestañaBasuras, new org.netbeans.lib.awtextra.AbsoluteConstraints(60,70, 260, 40));
           
            
            pestañaCumplimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/cumplimientoBorde.png")));
            pestañaAguas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoAguasCentro.png")));
            pestañaBasuras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fondos/manejoBasurasCentro.png")));
            
            Eficiencia.validate();
        }
        
        changePanel(Eficiencia, jPanelTecnicas);
    }//GEN-LAST:event_pestañaCumplimientoMouseClicked

    private void guardarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarDatosActionPerformed
        JOptionPane.showMessageDialog(null, "datos guardados correctamente");
    }//GEN-LAST:event_guardarDatosActionPerformed

    private void historicosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_historicosMouseClicked
        changePanel(Graficas, PanelHistoricos);
    }//GEN-LAST:event_historicosMouseClicked

    private void actualizarGraficaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_actualizarGraficaMouseClicked
        
        String periodo = panelCustomDesplegable_periodo.labelBotonT.getText();
        String Variable = panelCustomDesplegable_variable.labelBotonT.getText();
        if(PanelHistoricos.isVisible()){
            //adquirir datos historicos:
            
        }else if((PanelTiempoReal.isVisible())){
            //adqquirir datos en tiempo real
        }else{
            System.out.println("no se puede mostrar la grafica");
        }
        
        MachineComands comando = new MachineComands();
        //System.out.println("\"d:\\Juan David\\documentos semestre\\8 semestre\\intersemestral solar varini\\netBeans\\programaPrueba.exe\"");

        String Recibido = comando.executeCommand("\"d:\\Juan David\\documentos semestre\\8 semestre\\intersemestral solar varini\\netBeans\\programaPrueba.exe\"");
        System.out.println("el return del codigo es: "+Recibido);
        
        
    }//GEN-LAST:event_actualizarGraficaMouseClicked

    private void RealesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RealesMouseClicked
        changePanel(Graficas, PanelTiempoReal);
    }//GEN-LAST:event_RealesMouseClicked

    private void labelBotonGuardarConfigMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_labelBotonGuardarConfigMouseClicked
        
        //recibo el fondo seleccionado
        if(panelCustomDesplegableFondo.getLastPressed().equals("Fondo 1"))
        {
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo1.png"));
            fondo.setIcon(imagenFondo);
            fondoParasito.setIcon(imagenFondo);
            myConfiguration.setConfig("Fondos", "fondo1");
            
        }else if(panelCustomDesplegableFondo.getLastPressed().equals("Fondo 2"))
        {
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo2.png"));
            fondo.setIcon(imagenFondo);
            fondoParasito.setIcon(imagenFondo);
            myConfiguration.setConfig("Fondos","fondo2");
        }else if(panelCustomDesplegableFondo.getLastPressed().equals("Fondo 3"))
        {
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo3.png"));
            fondo.setIcon(imagenFondo);
            fondoParasito.setIcon(imagenFondo);
            myConfiguration.setConfig("Fondos","fondo3");
        }else if(panelCustomDesplegableFondo.getLastPressed().equals("Fondo 4")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo4.png"));
            fondo.setIcon(imagenFondo);
            fondoParasito.setIcon(imagenFondo);
            myConfiguration.setConfig("Fondos","fondo4");
        }
        
        //las demas configuraciones:
        boolean error=false;
        try{
            interfaz.wattHora=Double.parseDouble(jTextFieldCostoWatt.getText());
            myConfiguration.setConfig(Configuraciones.wattHora, String.valueOf(interfaz.wattHora));
            //System.out.println(interfaz.wattHora);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "error en el campo: \""+costoWat.getText()+"\" porfavor ingreselo nuevamente" );
            jTextFieldCostoWatt.setText("");//lo blanqueo
            error=true;
        }
        try{
            interfaz.metrosCubicos=Double.parseDouble(jTextFieldCostoAgua.getText());
            myConfiguration.setConfig(Configuraciones.metCubicoAgua, String.valueOf(interfaz.metrosCubicos));            
            //System.out.println(interfaz.metrosCubicos);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "error en el campo: \""+costoAgua.getText()+"\" porfavor ingreselo nuevamente" );
            jTextFieldCostoAgua.setText("");//lo blanqueo
            error=true;
        }
        
        
        try{
            interfaz.diaFacturacion=Integer.parseInt(jTextFieldDiaFacturacion.getText());
            myConfiguration.setConfig(Configuraciones.diaDeFacturacion, String.valueOf(interfaz.diaFacturacion));
            //System.out.println(interfaz.diaFacturacion);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "error en el campo: \""+diaDeFacturacion.getText()+"\" porfavor ingreselo nuevamente" );
            jTextFieldDiaFacturacion.setText("");//lo blanqueo
            error=true;
        }
        try{
            interfaz.numUsuarios=Integer.parseInt(jTextFieldNumPersonas.getText());
            myConfiguration.setConfig(Configuraciones.numUsuarios, String.valueOf(interfaz.numUsuarios));
            //System.out.println(interfaz.numUsuarios);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "error en el campo: \""+numeroPersonas.getText()+"\" porfavor ingreselo nuevamente" );
            jTextFieldNumPersonas.setText("");//lo blanqueo
            error=true;
        }
        interfaz.notificaciones=jCheckBoxNotificaciones.isSelected();
        if(notificaciones){
            myConfiguration.setConfig(Configuraciones.notificaciones, "si");
        }else{
            myConfiguration.setConfig(Configuraciones.notificaciones, "no");
        }
        if(!error){
            JOptionPane.showMessageDialog(null, "configuracion guardada");
        }
        
    }//GEN-LAST:event_labelBotonGuardarConfigMouseClicked

    private void guardarHorarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_guardarHorarioMouseClicked
        JOptionPane.showMessageDialog(null, "horario guardado");
    }//GEN-LAST:event_guardarHorarioMouseClicked

    private void pedirPermisoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pedirPermisoMouseClicked
        JOptionPane.showMessageDialog(null, panelCustomDesplegable_circuito.getLastPressed()+" activada");
    }//GEN-LAST:event_pedirPermisoMouseClicked

    private void jlabelDefaultMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlabelDefaultMouseClicked
        myFManejoDeConfiguraciones.config.setDefaultConfigs();
        myFManejoDeConfiguraciones.actualizarTodo();
        actualizarGuiConfiguraciones();
        fondoParasito.setIcon(imagenFondo);
    }//GEN-LAST:event_jlabelDefaultMouseClicked



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Agenda;
    private javax.swing.JPanel Aplicaciones;
    private javax.swing.JPanel Eficiencia;
    private javax.swing.JPanel Graficas;
    private javax.swing.JPanel Mensajes;
    private javax.swing.JPanel PanelHistoricos;
    private javax.swing.JPanel PanelTiempoReal;
    private javax.swing.JPanel Principal;
    private ClasesVarias.Botones.JlabelBoton Reales;
    private ClasesVarias.Botones.JlabelBoton actualizarGrafica;
    private ClasesVarias.Botones.CustomBoton appGestion;
    private ClasesVarias.Botones.CustomBoton appGraficas;
    private ClasesVarias.Botones.CustomBoton appHorarios;
    private ClasesVarias.Botones.CustomBoton atrasAgenda;
    private ClasesVarias.Botones.CustomBoton atrasConfiguraciones;
    private ClasesVarias.Botones.CustomBoton atrasEficiencia;
    private ClasesVarias.Botones.CustomBoton atrasGraficas;
    private ClasesVarias.Botones.CustomBoton config;
    private javax.swing.JDialog configuraciones;
    private javax.swing.JLabel costoAgua;
    private javax.swing.JLabel costoWat;
    private javax.swing.JLabel diaDeFacturacion;
    public static javax.swing.JLabel fondo;
    private javax.swing.JLabel fondoParasito;
    private ClasesVarias.Botones.CustomBoton guardarDatos;
    private ClasesVarias.Botones.JlabelBoton guardarHorario;
    private ClasesVarias.Botones.JlabelBoton historicos;
    private javax.swing.JCheckBox jCheckBox12;
    private javax.swing.JCheckBox jCheckBox15;
    private javax.swing.JCheckBox jCheckBox16;
    private javax.swing.JCheckBox jCheckBox17;
    private javax.swing.JCheckBox jCheckBox18;
    private javax.swing.JCheckBox jCheckBox19;
    private javax.swing.JCheckBox jCheckBox20;
    private javax.swing.JCheckBox jCheckBox21;
    private javax.swing.JCheckBox jCheckBox22;
    private javax.swing.JCheckBox jCheckBox23;
    private javax.swing.JCheckBox jCheckBox24;
    private javax.swing.JCheckBox jCheckBox25;
    private javax.swing.JCheckBox jCheckBox26;
    private javax.swing.JCheckBox jCheckBox27;
    private javax.swing.JCheckBox jCheckBox28;
    private javax.swing.JCheckBox jCheckBox39;
    private javax.swing.JCheckBox jCheckBox40;
    private javax.swing.JCheckBox jCheckBox41;
    private javax.swing.JCheckBox jCheckBox42;
    private javax.swing.JCheckBox jCheckBox43;
    private javax.swing.JCheckBox jCheckBox44;
    private javax.swing.JCheckBox jCheckBox45;
    private javax.swing.JCheckBox jCheckBox46;
    private javax.swing.JCheckBox jCheckBox47;
    private javax.swing.JCheckBox jCheckBox48;
    private javax.swing.JCheckBox jCheckBox49;
    private javax.swing.JCheckBox jCheckBox50;
    private javax.swing.JCheckBox jCheckBox51;
    private javax.swing.JCheckBox jCheckBox52;
    private javax.swing.JCheckBox jCheckBox53;
    private javax.swing.JCheckBox jCheckBox54;
    private javax.swing.JCheckBox jCheckBox55;
    private javax.swing.JCheckBox jCheckBox8;
    private javax.swing.JCheckBox jCheckBox9;
    private javax.swing.JCheckBox jCheckBoxNotificaciones;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanelAgua;
    private javax.swing.JPanel jPanelBasuras;
    private javax.swing.JPanel jPanelTecnicas;
    private javax.swing.JPanel jPanel_datos;
    private javax.swing.JPanel jPanel_texto;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextFieldCostoAgua;
    private javax.swing.JTextField jTextFieldCostoWatt;
    private javax.swing.JTextField jTextFieldDiaFacturacion;
    private javax.swing.JTextField jTextFieldNumPersonas;
    private ClasesVarias.Botones.JlabelBoton jlabelDefault;
    private javax.swing.JLabel labelActivarPermiso;
    private ClasesVarias.Botones.JlabelBoton labelBotonGuardarConfig;
    private javax.swing.JLabel labelGuardarHorario;
    private javax.swing.JLabel menuBar;
    private javax.swing.JLabel menuBarApp1;
    private javax.swing.JLabel menuBarApp2;
    private javax.swing.JLabel menuBarApp3;
    private javax.swing.JLabel menuBarApp4;
    private javax.swing.JLabel notificacioneCelular;
    private javax.swing.JLabel numeroPersonas;
    private javax.swing.JPanel panel;
    private javax.swing.JLabel panelAgenda;
    private javax.swing.JLabel panelAplicaciones;
    private ClasesVarias.Botones.PanelCustomDesplegable panelCustomDesplegableFondo;
    private ClasesVarias.Botones.PanelCustomDesplegable panelCustomDesplegable_circuito;
    private ClasesVarias.Botones.PanelCustomDesplegable panelCustomDesplegable_periodo;
    private ClasesVarias.Botones.PanelCustomDesplegable panelCustomDesplegable_variable;
    private javax.swing.JLabel panelMensajes;
    private ClasesVarias.Botones.JlabelBoton pedirPermiso;
    private javax.swing.JLabel periodoMostrado;
    private javax.swing.JLabel periodoMostrado1;
    private javax.swing.JLabel pestañaAgenda;
    private javax.swing.JLabel pestañaAguas;
    private javax.swing.JLabel pestañaBasuras;
    private javax.swing.JLabel pestañaCumplimiento;
    private ClasesVarias.PanelDeProgreso.ProgresBarPanel progresBarPanelAgua;
    private ClasesVarias.PanelDeProgreso.ProgresBarPanel progresBarPanelEnergia;
    private ClasesVarias.PanelDeProgreso.ProgresBarPanel progresBarPanelTemperatura;
    private ClasesVarias.Botones.CustomBoton salir;
    private javax.swing.JLabel seleccionarFondo;
    private javax.swing.JLabel textoDefault;
    private javax.swing.JLabel textoGuardarConfig1;
    private javax.swing.JLabel tituloAplicaciones;
    private javax.swing.JLabel tituloMensajes;
    private ClasesVarias.Botones.CustomBoton usuarios;
    private javax.swing.JLabel variableMostrada;
    // End of variables declaration//GEN-END:variables
}
