/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import java.util.Random;

/**
 *
 * @author roberto
 */
public class funcionesVarias {
    
    public static double [][] rangoSeno(double xInicial, double xFinal, double muestreo){
        
        int n = (int)((xFinal-xInicial)/muestreo); // "x" seria tiempo y "y" la medicion
        double [][] r = new double[n][2]; //se crea un arreglo de largo n para almacenar los valores de y
        for(int k=0;k<n;k++){            
            r[k][0] = xInicial+muestreo*k;
            r[k][1] = sin(r[k][0]);
            /*
            System.err.println(r[k][0]);
            System.err.println(r[k][1]);
            */
        }
        return r;
    }
    public static double [][] rangoCuadatica(double xInicial, double xFinal, double muestreo){
        int n = (int)((xFinal-xInicial)/muestreo); // "x" seria tiempo y "y" la medicion
        double [][] r = new double[n][2]; //se crea un arreglo de largo n para almacenar los valores de y
        for(int k=0;k<n;k++){            
            r[k][0] = xInicial+muestreo*k;
            r[k][1] = Math.pow(r[k][0],2);
            /*
            System.err.println(r[k][0]);
            System.err.println(r[k][1]);
            */
        }
        return r;
    }
    public static double [][] rangoRandom(double xInicial, double xFinal, double muestreo){
        int n = (int)((xFinal-xInicial)/muestreo); // "x" seria tiempo y "y" la medicion
        double [][] r = new double[n][2]; //se crea un arreglo de largo n para almacenar los valores de y
        Random rn = new Random();
        for(int k=0;k<n;k++){            
            r[k][0] = xInicial+muestreo*k;
            r[k][1] = rn.nextInt(10);
            /*
            System.err.println(r[k][0]);
            System.err.println(r[k][1]);
            */
        }
        return r;
    }
}
