/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author roberto
 */
public class MachineComands {
    
    /**
     * todos losparametros deben tener el caracter de direccion \ doble por convencion de java
     * ej: la direccion "c:\documentos" debe ser escrita "c:\\documentos"
     * 
     * el noblre del archivo debe incluir el .exe
     * 
     * @param direccion
     * @param nombrePrograma
     * @return
     */
    public String winCmdLine_ExecuteProgram(String direccion, String nombrePrograma){
        
        String comando="\""+direccion+"\\"+nombrePrograma+"\""; 
        //para añadir un solo caracter barra: "\" se utiliza doble barra: "\\"
        //para añadir un caracter tipo comillas: """  se remplaza por barra Comilla: "\""                        
        return comando;
    }
    
    public String executeCommand(String command) {

        StringBuffer output = new StringBuffer();

        Process myProcess;
        try {
                myProcess = Runtime.getRuntime().exec(command);
                myProcess.waitFor();//espera a que se termine el programa
                BufferedReader reader =
                    new BufferedReader(new InputStreamReader(myProcess.getInputStream()));

                String line = "";
                while ((line = reader.readLine())!= null) {
                        System.out.println("el read line es:"+reader.readLine());
                        output.append(line + "\n");
                }

        } catch (Exception e) {
                e.printStackTrace();
                System.out.println("problemas ejecutando el programa");
        }

        return output.toString();

    }
}
