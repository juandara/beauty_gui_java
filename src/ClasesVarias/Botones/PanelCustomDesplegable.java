/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.Botones;

import AppPackage.AnimationClass;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author roberto
 */
public class PanelCustomDesplegable extends JPanel 
{
    Thread myVerificador;
    Dimension panelSizeInicial;
    AnimationClass ObjetoParaAnimar;
    int numBotones;
    boolean estadoAbierto=false;
    
    public JlabelBoton labelBotonT;    
    public JlabelBoton labelBotonB;
    public JlabelBoton [] labelBotonMedios;
    
    public JLabel labelT;
    public JLabel labelB;
    public JLabel [] labelM ;
    
    public PanelCustomDesplegable() {
        this.ObjetoParaAnimar = new AnimationClass();
        
        //inicializadores:
        this.numBotones = 2;
        this.labelB = new JLabel();
        this.labelT = new JLabel();
        this.labelBotonB = new JlabelBoton("/botones/botonBajoN.png","/botones/botonBajoE.png","/botones/botonBajoP.png");
        this.labelBotonT = new JlabelBoton("/botones/botonTopN.png","/botones/botonTopE.png","/botones/botonTopP.png");
        
        //configuramos el panel
        panelSizeInicial=new Dimension(labelBotonB.getWidth(),labelBotonB.getHeight()*numBotones);
        super.setSize(panelSizeInicial);
        super.setOpaque(false);
        super.setLayout(null);
        super.add(this.labelT);
        super.add(this.labelB);
        super.add(this.labelBotonT);
        super.add(this.labelBotonB);
        
        //------para el primer boton---------
        //configuramos el label
        labelT.setSize(labelBotonT.size);
        labelT.setVisible(true);
        labelT.setEnabled(true);
        labelT.setOpaque(false);
        labelT.setForeground(new java.awt.Color(255, 255, 255));
        labelT.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        labelT.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelT.setText("textopruebaAlto");
        labelT.setLocation(0, 0);
        //configuramos el boton
        labelBotonT.setVisible(true);
        labelBotonT.setEnabled(true);
        labelBotonT.setLocation(0, 0);
        //--------------------------------------
        
        //------para el ultimo boton---------
        //configuramos el label
        labelB.setSize(labelBotonT.size);
        labelB.setVisible(false);
        labelB.setEnabled(false);
        labelB.setOpaque(false);
        labelB.setForeground(new java.awt.Color(255, 255, 255));
        labelB.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        labelB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelB.setText("textopruebaBajo");
        labelB.setLocation(0, 0);//debe estar detras del primero
        //configuramos el boton
        labelBotonB.setVisible(false);
        labelBotonB.setEnabled(false);
        labelBotonB.setLocation(0, 0);//debe estar detras del primero
        //--------------------------------------
        
        //relaciono la interfaz del boton del top y del bottom
        this.labelBotonT.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MouseClicked(evt);
            }
        });
        this.labelBotonB.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    MouseClicked(evt);
                }
        });
        
    }
    public PanelCustomDesplegable(String [] textos) {
        this.ObjetoParaAnimar = new AnimationClass();
        
        //inicializadores:
        this.numBotones = textos.length;
        this.labelB = new JLabel();
        this.labelT = new JLabel();
        this.labelBotonB = new JlabelBoton("/botones/botonBajoN.png","/botones/botonBajoE.png","/botones/botonBajoP.png");
        this.labelBotonT = new JlabelBoton("/botones/botonTopN.png","/botones/botonTopE.png","/botones/botonTopP.png");
        
        //inicializadores de los botones medios:
        if(numBotones>2){
           this.labelM = new JLabel[numBotones-2];
           this.labelBotonMedios = new JlabelBoton[numBotones-2];
           for(int b=0; b<numBotones-2;b++){
               this.labelM[b] = new JLabel();
               this.labelBotonMedios[b] = new JlabelBoton("/botones/botonMedioN.png","/botones/botonMedioE.png","/botones/botonMedioP.png");
           }
        }
        
        //configuramos el panel
        panelSizeInicial=new Dimension(labelBotonT.getWidth(),labelBotonT.getHeight()*numBotones);
        super.setSize(panelSizeInicial);
        super.setOpaque(false);
        super.setLayout(null);
        super.add(this.labelT);
        super.add(this.labelB);
        super.add(this.labelBotonT);
        super.add(this.labelBotonB);
        if(numBotones>2){
           for(int c=0; c<numBotones-2;c++){
               super.add(labelM[c]);
               super.add(labelBotonMedios[c]);
           }
        }
        
        //------para el primer boton---------
        //configuramos el label
        labelT.setSize(labelBotonT.size);
        labelT.setVisible(true);
        labelT.setEnabled(true);
        labelT.setOpaque(false);
        labelT.setForeground(new java.awt.Color(255, 255, 255));
        labelT.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        labelT.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelT.setText(textos[0]);
        labelT.setLocation(0, 0);
        //configuramos el boton
        labelBotonT.setVisible(true);
        labelBotonT.setEnabled(true);
        labelBotonT.setLocation(0, 0);
        //--------------------------------------
        
        //------para el ultimo boton---------
        //configuramos el label
        labelB.setSize(labelBotonT.size);
        labelB.setVisible(false);
        labelB.setEnabled(false);
        labelB.setOpaque(false);
        labelB.setForeground(new java.awt.Color(255, 255, 255));
        labelB.setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
        labelB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelB.setText(textos[numBotones-1]);
        labelB.setLocation(0, 0);//debe estar detras del primero
        //configuramos el boton
        labelBotonB.setVisible(false);
        labelBotonB.setEnabled(false);
        labelBotonB.setLocation(0, 0);//debe estar detras del primero
        //--------------------------------------
        
        //relaciono la interfaz del boton del top y del bottom
        this.labelBotonT.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MouseClicked(evt);
            }
        });
        this.labelBotonB.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(estadoAbierto){
                    String aux = labelT.getText();
                    labelT.setText(labelB.getText());
                    labelB.setText(aux);
                }
                MouseClicked(evt);
            }
        });
        
        //para la configuracion botones intermedios
        for(int i=0;i<numBotones-2;i++){
            //configuramos el label
            labelM[i].setSize(labelBotonT.size);
            labelM[i].setVisible(false);
            labelM[i].setEnabled(false);
            labelM[i].setOpaque(false);
            labelM[i].setForeground(new java.awt.Color(255, 255, 255));
            labelM[i].setFont(new java.awt.Font("Franklin Gothic Book", 0, 14)); // NOI18N
            labelM[i].setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            labelM[i].setText(textos[i+1]);
            labelM[i].setLocation(0, 0);//debe estar detras del primero
            
            //configuramos el boton
            labelBotonMedios[i].setVisible(false);
            labelBotonMedios[i].setEnabled(false);
            labelBotonMedios[i].setLocation(0, 0);//debe estar detras del primero
            //--------------------------------------
            
            //añado los eventos de clickeo
            this.labelBotonMedios[i].addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    if(estadoAbierto){
                        String aux = labelT.getText();
                        for(int z=0; z<numBotones-2;z++){
                           if(labelBotonMedios[z].equals(evt.getComponent())){
                               labelT.setText(labelM[z].getText());
                               labelM[z].setText(aux);
                           }
                        }
                    }
                    MouseClicked(evt);
                }
            });
        }
        
    }
    
    void bajarBotonCompleto(JLabel label,JlabelBoton botonLabel,int velocidad,int yFinal){
        
        ObjetoParaAnimar.jLabelYDown(label.getY(), yFinal, 10, velocidad, label);
        ObjetoParaAnimar.jLabelYDown(botonLabel.getY(), yFinal, 10, velocidad, botonLabel);
    }
    void subirBotonCompleto(JLabel label,JlabelBoton botonLabel,int velocidad,int yFinal){
        
        ObjetoParaAnimar.jLabelYUp(label.getY(), yFinal, 10, velocidad, label);
        ObjetoParaAnimar.jLabelYUp(botonLabel.getY(), yFinal, 10, velocidad, botonLabel);
        
    }
    @Override
    public Dimension getPreferredSize() {
        return panelSizeInicial;
    };
    public String getLastPressed() {
        return labelT.getText();
    };
    public void toggleState(){
        if(this.estadoAbierto==true){
            this.estadoAbierto=false;
            //apago el boton en cuestion (el boton y el  label)
            this.labelBotonB.setVisible(false);
            this.labelBotonB.setEnabled(false);
            this.labelB.setVisible(false);
            this.labelB.setEnabled(false);
            if(numBotones>2){
                for(int l=0; l<numBotones-2;l++){
                   this.labelM[l].setVisible(false);
                   this.labelM[l].setEnabled(false);
                   this.labelBotonMedios[l].setEnabled(false);
                   this.labelBotonMedios[l].setVisible(false);
                }
            }
            
        }else{//cerrado: estadoAbierto=false;
            this.estadoAbierto=true;
             //enciendo el boton el cuestion (label y boton)
            this.labelBotonB.setVisible(true);
            this.labelBotonB.setEnabled(true);
            this.labelB.setVisible(true);
            this.labelB.setEnabled(true);
            if(numBotones>2){
                for(int z=0; z<numBotones-2;z++){
                   this.labelM[z].setVisible(true);
                   this.labelM[z].setEnabled(true);
                   this.labelBotonMedios[z].setVisible(true);
                   this.labelBotonMedios[z].setEnabled(true);
                }
            }
        }
    }
   
    
    
    //mouse events:
    private void MouseClicked(MouseEvent evt) {
        if(estadoAbierto){
           
            subirBotonCompleto(labelB, labelBotonB, 3, labelBotonT.getY());
            if(numBotones>2){
                for(int j=0; j<numBotones-2;j++){
                   subirBotonCompleto(this.labelM[j], this.labelBotonMedios[j], 3, labelBotonT.getY());
                }
            }
            //el hilo verificador del estado de los botones para el auto toggle
            myVerificador = new Thread(new verificador(this));
            myVerificador.start();
            
        }else{
            toggleState();
            bajarBotonCompleto(labelB, labelBotonB, 3, labelBotonT.getHeight()*(numBotones-1));
            if(numBotones>2){
                for(int k=0; k<numBotones-2;k++){
                   bajarBotonCompleto(labelM[k], labelBotonMedios[k], 3,  labelBotonT.getHeight()*(k+1));
                }
            }
        }

    }

    
    public void action(ActionEvent e) {
        
    }
        
}
