/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.Botones;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author roberto
 */
public class CustomBoton extends JButton implements MouseListener  {
    
    Dimension size = new Dimension(250,120);
    
    boolean hover=false;
    boolean presionado= false;
    
    //default images:
    ImageIcon imagenNormal; 
    ImageIcon imagenPresionado;
    ImageIcon imagenEncima;
    
    String urlNormal="/botones/logo_normal.png";
    String urlEncima="/botones/logo_presionado.png"; 
    String urlPresionado ="/botones/logo_encima.png";
    
    //default text
    String texto=""; 
    public CustomBoton(){
              
        imagenNormal = new ImageIcon(getClass().getResource(urlNormal));
        imagenEncima = new ImageIcon(getClass().getResource(urlEncima));
        imagenPresionado = new ImageIcon(getClass().getResource(urlPresionado));
        
        this.size= new Dimension(imagenNormal.getIconWidth(), imagenNormal.getIconHeight());
        
        
        //----icon image-----
        super.setIcon(imagenNormal);
        super.setSize(size);
        
        //----visualizar----
        super.setVisible(true);
        super.setFocusable(true);
        
        //------transparencia------
        super.setContentAreaFilled(false);
        super.setOpaque(false);
        
        super.setText(this.texto);
        super.addMouseListener(this);          
    }
    public CustomBoton(String urlNormal, String urlEncima, String urlPresionado){
              
        
        imagenNormal = new ImageIcon(getClass().getResource(urlNormal));
        imagenEncima = new ImageIcon(getClass().getResource(urlEncima));
        imagenPresionado = new ImageIcon(getClass().getResource(urlPresionado));
        
        size= new Dimension(imagenNormal.getIconWidth(), imagenNormal.getIconHeight());
        
        //----icon image-----
        super.setIcon(imagenNormal);
        
        super.setSize(size);
        
        //----visualizar----
        super.setVisible(true);
        super.setFocusable(true);
        
        //------transparencia------
        super.setContentAreaFilled(false);
        super.setOpaque(false);
        
        super.setText(this.texto);
        super.addMouseListener(this);          
    }
    
    
    public void setIcons(String normal, String encima, String presionado){
        this.urlNormal=normal;
        this.urlEncima=encima;
        this.urlPresionado=presionado;
        
        imagenNormal = new ImageIcon(getClass().getResource(urlNormal));
        imagenEncima = new ImageIcon(getClass().getResource(urlEncima));
        imagenPresionado = new ImageIcon(getClass().getResource(urlPresionado));
        
        super.setIcon(imagenNormal);
        
    }
    @Override
    public Dimension getPreferredSize(){
        
        return size;
    }
    @Override
    public Dimension getMaximumSize(){
        return size;
    }
    @Override
    public Dimension getMinimumSize(){
        return size;
    }

    @Override
    public void setText(String texto){
        this.texto = texto;
    }
    public String getButtonText(){
        return this.texto;
    }
    

    //mouse events:
    @Override
    public void mouseEntered(MouseEvent e) {                                        
        hover=true;
        super.setIcon(imagenEncima);
    }
    @Override
    public void mouseExited(MouseEvent e) {                                        
        hover=false;
        super.setIcon(imagenNormal);        
    }
    @Override
    public void mousePressed(MouseEvent e) {                                        
        presionado=true;
        super.setIcon(imagenPresionado);        
    }
    @Override
    public void mouseReleased(MouseEvent e) {                                        
        presionado=false;
        super.setIcon(imagenEncima);        
    }
    @Override
    public void mouseClicked(MouseEvent e) {}

    
}
