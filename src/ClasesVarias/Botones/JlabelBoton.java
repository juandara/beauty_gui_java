/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.Botones;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;


/**
 *
 * @author roberto
 */
public class JlabelBoton extends JLabel implements MouseListener,ActionListener  {
    
    Dimension size = new Dimension(250,120);
    
    boolean hover=false;
    boolean presionado= false;
    
    //default images:
    ImageIcon imagenNormal; 
    ImageIcon imagenPresionado;
    ImageIcon imagenEncima;
    
    
    
    String urlNormal="/botones/boton1N.png";
    String urlEncima="/botones/boton1E.png"; 
    String urlPresionado ="/botones/boton1P.png";
    
    //default text
    String texto=""; 
    public JlabelBoton (){
              
        imagenNormal = new ImageIcon(getClass().getResource(urlNormal));
        imagenEncima = new ImageIcon(getClass().getResource(urlEncima));
        imagenPresionado = new ImageIcon(getClass().getResource(urlPresionado));
        
        this.size= new Dimension(imagenNormal.getIconWidth(), imagenNormal.getIconHeight());
        
        //----icon image-----
        super.setIcon(imagenNormal);
        super.setSize(size);
        
        
        //----visualizar----
        super.setVisible(true);
        super.setFocusable(true);
        
       
        
        super.setText(this.texto);
        super.addMouseListener(this);
        
    
    }
    
    public JlabelBoton(String urlNormal, String urlEncima, String urlPresionado){
              
        
        imagenNormal = new ImageIcon(getClass().getResource(urlNormal));
        imagenEncima = new ImageIcon(getClass().getResource(urlEncima));
        imagenPresionado = new ImageIcon(getClass().getResource(urlPresionado));
        
        size= new Dimension(imagenNormal.getIconWidth(), imagenNormal.getIconHeight());
        
        //----icon image-----
        super.setIcon(imagenNormal);
        
        super.setSize(size);
        
        //----visualizar----
        super.setVisible(true);
        super.setFocusable(true);
        
        super.setText(this.texto);
        super.addMouseListener(this);    
    }
    //mouse events:
    @Override
    public void mouseEntered(MouseEvent e) {                                        
        hover=true;
        super.setIcon(imagenEncima);
    }
    @Override
    public void mouseExited(MouseEvent e) {                                        
        hover=false;
        super.setIcon(imagenNormal);        
    }
    @Override
    public void mousePressed(MouseEvent e) {                                        
        presionado=true;
        super.setIcon(imagenPresionado);        
    }
    @Override
    public void mouseReleased(MouseEvent e) {                                        
        presionado=false;
        super.setIcon(imagenEncima);        
    }
    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void actionPerformed(ActionEvent ev) {}
}
