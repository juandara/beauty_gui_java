/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.PanelDeProgreso;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author roberto
 */
public class ProgresBarPanel extends JPanel{
    public int progreso=30; //en porcentaje
    int grosor=10;
    String nombre="Default"; //este sera el texto mostrado
    //set opaque this

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(false); //hacemos opaco el panel

    }
    
    public void setText(String nombre){
        this.nombre=nombre;
    }
    public void updateProgress (int progreso){
        if((progreso<=100)&&(progreso>=0)){
            this.progreso=progreso;
        }else{
            this.progreso=100;
        }
        repaint();
        super.validate();
    }
    public void updateGrosor (int grosor){
        this.grosor=grosor;
        repaint();
    }
    @Override
    public void paint(Graphics g){
        //----------------------primer metodo--------------------------------------
        /*
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);//usamos antialiasing para que la imagen se vea mejor
        
        g2.translate(this.getWidth()/2, this.getHeight()/2); //set the origin of g2 in the origin of the panel
        g2.rotate(Math.toRadians(0));//para que el grado cero empiece en 90 (la rotacion es en sentido de las manecillas positiva por eso es -90)
        
        //Creamos el arco y sus caracteristicas:
        Arc2D.Float arc = new Arc2D.Float(Arc2D.PIE);
        arc.setFrameFromCenter(new Point(0, 0), new Point(this.getWidth()/2,this.getHeight()/2));
        arc.setAngleStart(90); //angulo cartesiano de inicio
        arc.setAngleExtent(-progreso*3.6);//distancia angular recorrida por el arco ()
        
        //Configuracion de forma y color:
        g2.setColor(Color.red); //selecciona el color
        g2.draw(arc); //solo dibuja el contorno
        g2.fill(arc); //rellena en controno
        
        ----
        //Creamos en circulo interno y sus caracteristicas:
        Ellipse2D inCircle = new Ellipse2D.Float(0, 0, (this.getWidth()/2)-grosor, (this.getWidth()/2)-grosor);
        inCircle.setFrameFromCenter(new Point(0, 0),  new Point((this.getWidth()/2)-grosor,(this.getHeight()/2)-grosor));
        
        //Configuracion de forma y color:
        g2.setColor(Color.white); //selecciona el color
        g2.draw(inCircle); //solo dibuja el contorno
        g2.fill(inCircle); //rellena en controno
        -----
        //Creamos el texto que se mostrara en question:
        g2.setColor(Color.white); //selecciona el color
        g.setFont(new Font("verdana", Font.BOLD, (grosor-5)));
        FontMetrics fm = g2.getFontMetrics();
        Rectangle2D r = fm.getStringBounds(nombre + ": "+ progreso + "%", g);
        int x=((0-(int)r.getWidth())/2);
        int y=((0-(int)r.getHeight())/2)+fm.getAscent();
        
        g2.drawString(nombre + ": "+ progreso + "%", x, y);
        */
        //-----------------------------------------------------------------------------------
        
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g; //la grafica g se guarda como grafica de 2 d
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);//usamos antialiasing para que la imagen se vea mejor

        
        AffineTransform at = new AffineTransform(); //las tipo transform es para crear figuras simples
        AffineTransform save_ctx= g2.getTransform();
        AffineTransform current_ctx= new AffineTransform();
        
        current_ctx.concatenate(at);
        current_ctx.translate(getWidth()/2,getHeight()/2);
        //current_ctx.rotate(Math.toRadians(270));
        
        g2.transform(current_ctx);
        g2.setColor(new Color((int)2.55*progreso,(int)(255-2.55*progreso),0));
        g2.setStroke(new BasicStroke(grosor, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)); //this is decoration for the line
        g2.drawArc(-(getWidth()-grosor)/2, -(getHeight()-grosor)/2, getWidth()-grosor, getHeight()-grosor, 180, -(int)(progreso*3.6));//the progress of the line
        
        
        AffineTransform current_ctx2= new AffineTransform();
        g2.transform(current_ctx2);
        
        //Creamos el texto que se mostrara en question:
        g2.setColor(new Color(232, 235, 240)); //selecciona el color
        g.setFont(new Font("alegrian", Font.BOLD, (grosor+5)));
        FontMetrics fm = g2.getFontMetrics();
        Rectangle2D r = fm.getStringBounds(nombre + ": "+ progreso + "%", g);
        int x=((0-(int)r.getWidth())/2);
        int y=((0-(int)r.getHeight())/2)+fm.getAscent();
        g2.drawString(nombre + ": "+ progreso + "%", x, y);
        
        g2.transform(save_ctx);
    }

}
