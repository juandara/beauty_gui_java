/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.PanelDeProgreso;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */

public class ProgresBarRunnable implements Runnable{

    static int progreso; //a partir de este hacemos el calculo del porcentaje
    int incremento;
    ProgresBarPanel panelBar;
    
    
    public ProgresBarRunnable (ProgresBarPanel panelBar,int incremento){
        this.panelBar=panelBar;
        this.incremento=incremento;
    }
    public void actualizarValor(int progreso){
        ProgresBarRunnable.progreso=progreso;
    }
    @Override
    public void run(){
        while(true){
            if(progreso>=100){
                
            }else{
                progreso=progreso+incremento;
                panelBar.updateProgress(progreso);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ProgresBarPanel.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("error en el thread runnable progress bar: del panel "+panelBar.nombre);
                } 
            }
            

        }
    }   
}

