/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.Configuraciones;

import appPackage.interfaz;
import static appPackage.interfaz.fondo;
import static appPackage.interfaz.imagenFondo;


public class ManejoDeConfiguraciones {
    
    public Configuraciones config;
    
    public ManejoDeConfiguraciones(){
        //utilizo los metodos de lectura del configuraciones
        config = new Configuraciones();
    }

    /**
     * set the icon from the jlabel "fondo" to the one defined by configurations.
     */
    public void actualizarEstadoNotificaciones(){
        interfaz.notificaciones = config.readConfig(Configuraciones.notificaciones).equals("si");
    }
    public void actualizarWattHora(){
        interfaz.wattHora=Double.parseDouble(config.readConfig(Configuraciones.wattHora));
    }
    public void actualizarMetrosCubicosAgua(){
        interfaz.metrosCubicos=Double.parseDouble(config.readConfig(Configuraciones.metCubicoAgua));
    }
    public void actualizarNumeroDeUsuarios(){
        interfaz.numUsuarios=Integer.parseInt(config.readConfig(Configuraciones.numUsuarios));
    }
    public void actualizarDiaDeFacturacion(){
        interfaz.diaFacturacion=Integer.parseInt(config.readConfig(Configuraciones.diaDeFacturacion));
    }
    
    public void actualizarFondo(){
        
        if(config.readConfig(Configuraciones.fondos).equals("fondo1")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo1.png"));
            fondo.setIcon(imagenFondo);
        }else if(config.readConfig(Configuraciones.fondos).equals("fondo2")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo2.png"));
            fondo.setIcon(imagenFondo);
        }else if(config.readConfig(Configuraciones.fondos).equals("fondo3")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo3.png"));
            fondo.setIcon(imagenFondo);
        }else if(config.readConfig(Configuraciones.fondos).equals("fondo4")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo4.png"));
            fondo.setIcon(imagenFondo);
        }else{
            System.out.println("ClasesVarias.setFondo no aplicado");
        }        
    }
    public void actualizarTodo(){
        
        interfaz.notificaciones = config.readConfig(Configuraciones.notificaciones).equals("si");
        
        interfaz.wattHora=Double.parseDouble(config.readConfig(Configuraciones.wattHora));
        
        interfaz.metrosCubicos=Double.parseDouble(config.readConfig(Configuraciones.metCubicoAgua));
        
        interfaz.numUsuarios=Integer.parseInt(config.readConfig(Configuraciones.numUsuarios));
        
        interfaz.diaFacturacion=Integer.parseInt(config.readConfig(Configuraciones.diaDeFacturacion));
        
        if(config.readConfig(Configuraciones.fondos).equals("fondo1")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo1.png"));
            fondo.setIcon(imagenFondo);
        }else if(config.readConfig(Configuraciones.fondos).equals("fondo2")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo2.png"));
            fondo.setIcon(imagenFondo);
        }else if(config.readConfig(Configuraciones.fondos).equals("fondo3")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo3.png"));
            fondo.setIcon(imagenFondo);
        }else if(config.readConfig(Configuraciones.fondos).equals("fondo4")){
            imagenFondo = new javax.swing.ImageIcon(getClass().getResource("/fondos/fondo4.png"));
            fondo.setIcon(imagenFondo);
        }else{
            System.out.println("ClasesVarias.setFondo no aplicado");
        } 
    }
}
