/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesVarias.Configuraciones;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author roberto
 */
public class Configuraciones {
    
    public static Properties myPropiedades = new Properties();
    
    //los identificadores de las propiedades definidas:
    public final static String notificaciones = "notificacionesActivadas";
    public final static String wattHora="costoWattHora";
    public final static String metCubicoAgua="costoMetrosCubicos";
    public final static String numUsuarios = "numeroDeUsuarios";
    public final static String diaDeFacturacion = "diaDeFacturacion";
    public final static String fondos = "Fondos";
    
    public void setDefaultConfigs(){
        try{
            myPropiedades.setProperty(
                    notificaciones, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    "si"); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
        try{
            myPropiedades.setProperty(
                    wattHora, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    "6400.45"); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
        try{
            myPropiedades.setProperty(
                    metCubicoAgua, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    "4500.676"); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
        try{
            myPropiedades.setProperty(
                    numUsuarios, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    "5"); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
        try{
            myPropiedades.setProperty(
                    diaDeFacturacion, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    "15"); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
        try{
            myPropiedades.setProperty(
                    fondos, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    "fondo4"); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
    }
    
    public void setConfig(String idPropedad, String idValorDeLaPropiedad){
        try{
            myPropiedades.setProperty(
                    idPropedad, //la variable referente a la propiedad que deseo mostrar en el documento de propieades
                    idValorDeLaPropiedad); //como tal la popiedad guardada
            myPropiedades.store(new FileOutputStream("configData"//nombre del documento a crear
            ), null);
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.setFondoConfig() error");
        }
    }
    public String readConfig(String idPropiedad){
        String line="";
        try{
            myPropiedades.load(new FileInputStream("configData"));
            line=myPropiedades.getProperty(idPropiedad);
            
        }catch(IOException e){
            System.out.println("ClasesVarias.Configuraciones.ReadFondoConfig() error");
        }
        return line;
    }
}
