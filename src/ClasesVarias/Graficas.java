package ClasesVarias;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Graficas {
    
    public JFreeChart grafica;      //creo un area tipo JFreChart para colocar las graficas
    public String titulo;      //aqui guardo mi titulo de la grafica
    public String tituloX;     //aqui el del eje x
    public String tituloY;     //aqui el del eje y
    
    ChartPanel myChartPanel;    //
    XYSeriesCollection miColeccion;   //objeto con metodos para almacenar los paquetes de datos
    XYSeries serieDatos;        //objeto con metodos metodo para añadir los datos individuales
    
    public Graficas(String titulo, String tituloX, String tituloY){
        
        this.titulo=titulo;
        this.tituloX=tituloX;
        this.tituloY=tituloY;
        //grafica = ChartFactory.createXYLineChart(this.titulo, this.tituloX, this.tituloY, datos, PlotOrientation.VERTICAL, true, true, true);
       
        
    }
    //se crea el trazo de la funcion dada 
    public void agregarDatos(String id, double[][] points /*x,y*/){
        /*
        miColeccion = new XYSeriesCollection();
        serieDatos= new XYSeries(id);
        int n=points.length;
        for(int i=0;i<n;i++){
            this.serieDatos.add(points[i][0],points[i][1]);
        }
        miColeccion.addSeries(this.serieDatos);
        */
    }
    public void agregar1Dato(double datoX, double datoY) {
        
    }
    public void crearLineChart(){
        /*
        // Genero la grafica con las caracteristicas:
        grafica = ChartFactory.createXYLineChart(
        titulo, // Title
        tituloX, // x-axis Label
        tituloY, // y-axis Label
        miColeccion, // Dataset
        PlotOrientation.VERTICAL, // Plot Orientation
        true, // Show Legend
        true, // Use tooltips
        false // Configure chart to generate URLs?
        );
        */
    }
    public void agregarAPanel(JPanel panel){
        ChartPanel CP = new ChartPanel(grafica);
        panel.setLayout(new java.awt.BorderLayout());
        panel.add(CP,BorderLayout.CENTER);
        panel.validate();
    }
 }
